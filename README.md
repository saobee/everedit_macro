#everedit_macro
编辑器evedit的小扩展。

##Mysql Batch Insert
将mysql导出的sql文件每条insert调整成整句插入，加快导入效率。
第一版尚未判断数据大小，**若数据量过大超过max_post_size会插入失败**。
* * *
##CSS Compress
压缩CSS，去掉多余的空格和换行。